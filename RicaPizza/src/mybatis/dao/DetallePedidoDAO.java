package mybatis.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import ricapizza.model.DetallePedido;

public interface DetallePedidoDAO {
    public List<DetallePedido> obtenerDetallesPedido(Integer idPedido);
    public List<DetallePedido> obtenerDetallePedido(@Param("idPedido") Integer idPedido, @Param("codigo") String codigo);
    public List<DetallePedido> existeProducto(String codigo);
    public int crearDetallePedido(DetallePedido detallePedido);
    public int actualizarDetallePedido(DetallePedido detallePedido);
    public int eliminarDetallePedido(@Param("idPedido") Integer idPedido, @Param("codigo") String codigo);
}
