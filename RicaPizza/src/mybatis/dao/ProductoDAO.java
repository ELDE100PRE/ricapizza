package mybatis.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import ricapizza.model.Producto;

public interface ProductoDAO {
    public List<Producto> obtenerProductos();
    public List<Producto> obtenerProductosPedido(Integer idPedido);
    public List<Producto> obtenerProductosBusqueda(@Param("busqueda") String busqueda);
    public List<Producto> existeProducto(String codigo);
    public Producto obtenerProducto(String codigo);
    public int crearProducto(Producto producto);
    public int editarProducto(Producto producto);
    public int eliminarProducto(String codigo);
}
