package mybatis.dao;

import java.util.List;
import ricapizza.model.Pedido;

public interface PedidoDAO {
    public List<Pedido> obtenerPedidos();
    public List<Pedido> obtenerPedidosCliente(Integer idCliente);
    public List<Pedido> obtenerPedidosFecha(String fecha);
    public List<Pedido> obtenerPedidosEstado(String estado);
    public Pedido obtenerPedido(Integer idPedido);
    public int crearPedido(Pedido pedido);
    public int actualizarPedido(Pedido pedido);
    public int eliminarPedido(Integer idPedido);
}
