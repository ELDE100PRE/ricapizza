package mybatis.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import ricapizza.model.Empleado;

public interface EmpleadoDAO {
    public List<Empleado> obtenerEmpleados();
    public List<Empleado> obtenerEmpleadosBusqueda(@Param("busqueda") String busqueda);
    public List<Empleado> existeEmpleado(@Param("telefono") String telefono, @Param("email") String email);
    public Empleado obtenerEmpleado(@Param("usuario") String usuario, @Param("contrasenia") String contrasenia);
    public int crearEmpleado(Empleado empleado);
    public int editarEmpleado(Empleado empleado);
    public int cambiarContrasenia(Empleado empleado);
    public int eliminarEmpleado(Integer idEmpleado);
}
