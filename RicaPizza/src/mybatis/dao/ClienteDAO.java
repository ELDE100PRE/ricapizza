package mybatis.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import ricapizza.model.Cliente;

public interface ClienteDAO {
    public List<Cliente> obtenerClientes();
    public List<Cliente> obtenerClientesBusqueda(@Param("busqueda") String busqueda);
    public List<Cliente> existeCliente(@Param("telefono") String telefono, @Param("email") String email);
    public Cliente obtenerCliente(Integer idCliente);
    public int crearCliente(Cliente cliente);
    public int editarCliente(Cliente cliente);
    public int eliminarCliente(Integer idCliente);
}
