package ricapizza.controller;

import java.net.URL;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import mybatis.MyBatisUtil;
import mybatis.dao.EmpleadoDAO;
import org.apache.ibatis.session.SqlSession;
import ricapizza.Util;
import ricapizza.model.Empleado;

public class EmpleadosController implements Initializable {
    private Stage stage;
    private Empleado empleadoLogin;
    private Empleado empleado;
    private String telefonoOriginal;
    private String emailOriginal;
    
    @FXML
    private TableView<Empleado> empleados;
    @FXML
    private TableColumn<Empleado, String> nombres;
    @FXML
    private TableColumn<Empleado, String> apellidos;
    @FXML
    private TableColumn<Empleado, String> telefono;
    @FXML
    private TableColumn<Empleado, String> email;
    @FXML
    private TableColumn<Empleado, String> direccion;
    @FXML
    private TableColumn<Empleado, Number> codigopostal;
    @FXML
    private TableColumn<Empleado, String> ciudad;
    @FXML
    private TextField nombresT;
    @FXML
    private TextField apellidosT;
    @FXML
    private TextField telefonoT;
    @FXML
    private TextField emailT;
    @FXML
    private TextField direccionT;
    @FXML
    private TextField codigopostalT;
    @FXML
    private TextField ciudadT;
    @FXML
    private TextField contraseniaT;
    @FXML
    private TextField buscar;
    @FXML
    private Button contrasenia;
    @FXML
    private GridPane formulario;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        empleados.setItems(FXCollections.observableArrayList(obtenerEmpleados()));
        nombres.setCellValueFactory(data -> data.getValue().getNombresProperty());
        apellidos.setCellValueFactory(data -> data.getValue().getApellidosProperty());
        telefono.setCellValueFactory(data -> data.getValue().getTelefonoProperty());
        email.setCellValueFactory(data -> data.getValue().getEmailProperty());
        direccion.setCellValueFactory(data -> data.getValue().getDireccionProperty());
        codigopostal.setCellValueFactory(data -> data.getValue().getCodigopostalProperty());
        ciudad.setCellValueFactory(data -> data.getValue().getCiudadProperty());
        empleados.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null) {
                empleado = newValue;
                llenarFormulario();
            }
        });
        buscar.textProperty().addListener((observable, oldValue, newValue) -> {
            empleados.setItems(FXCollections.observableArrayList(obtenerEmpleados(newValue)));
        });
        buscar.setText("");
        contraseniaT.managedProperty().bind(contraseniaT.visibleProperty());
        contrasenia.managedProperty().bind(contrasenia.visibleProperty());
        formulario.setDisable(true);
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Empleado getEmpleadoLogin() {
        return empleadoLogin;
    }

    public void setEmpleadoLogin(Empleado empleadoLogin) {
        this.empleadoLogin = empleadoLogin;
    }
        
    private void llenarFormulario() {
        formulario.setDisable(false);
        nombresT.setText(empleado.getNombres());
        apellidosT.setText(empleado.getApellidos());
        telefonoT.setText(empleado.getTelefono());
        emailT.setText(empleado.getEmail());
        direccionT.setText(empleado.getDireccion());
        codigopostalT.setText(empleado.getCodigopostal().toString());
        ciudadT.setText(empleado.getCiudad());
        contraseniaT.setText("");
        contraseniaT.setEditable(false);
        contraseniaT.setVisible(false);
        contrasenia.setDisable(false);
        contrasenia.setVisible(true);
    }
    
    @FXML
    private void limpiarFormulario() {
        formulario.setDisable(true);
        empleados.getSelectionModel().clearSelection();
        empleado = null;
        nombresT.setText("");
        apellidosT.setText("");
        telefonoT.setText("");
        emailT.setText("");
        direccionT.setText("");
        codigopostalT.setText("");
        ciudadT.setText("");
        contraseniaT.setText("");
        contraseniaT.setEditable(true);
        contraseniaT.setVisible(true);
        contrasenia.setDisable(true);
        contrasenia.setVisible(false);
    }
    
    private void actualizarEmpleados() {
        empleados.getItems().clear();
        if(buscar.getText().trim().isEmpty()) {
            empleados.setItems(FXCollections.observableArrayList(obtenerEmpleados()));
        } else {
            empleados.setItems(FXCollections.observableArrayList(obtenerEmpleados(buscar.getText())));
        }
    }
    
    @FXML
    private void nuevoEmpleado() {
        empleados.getSelectionModel().clearSelection();
        empleado = new Empleado(null, "", "", "", "", "", null, "", "");
        llenarFormulario();
        codigopostalT.setText("");
        contraseniaT.setText("");
        contraseniaT.setEditable(true);
        contraseniaT.setVisible(true);
        contrasenia.setDisable(true);
        contrasenia.setVisible(false);
    }
    
    @FXML
    private void guardarEmpleado() {
        if(empleado == null) {
            Util.dialogo(Alert.AlertType.ERROR, "Selecciona o crea un empleado antes de continuar");
            return;
        }
        if(nombresT.getText().trim().isEmpty()) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce el/los nombre(s) del empleado");
            return;
        }
        empleado.setNombres(nombresT.getText());
        if(apellidosT.getText().trim().isEmpty()) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce el/los apellido(s) del empleado");
            return;
        }
        empleado.setApellidos(apellidosT.getText());
        if(telefonoT.getText().trim().isEmpty() || telefonoT.getText().trim().length() != 10 ||
                !Util.esNumero(telefonoT.getText().trim())) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce un télefono del empleado valido");
            return;
        }
        if(empleado.getIdEmpleado() != null) {
            telefonoOriginal = empleado.getTelefono();
        }
        empleado.setTelefono(telefonoT.getText().trim());
        if(emailT.getText().trim().isEmpty() || !Util.esCorreo(emailT.getText().trim())) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce un email del empleado valido");
            return;
        }
        if(empleado.getIdEmpleado() != null) {
            emailOriginal = empleado.getEmail();
        }
        empleado.setEmail(emailT.getText().trim());
        if(direccionT.getText().trim().isEmpty() || !Util.esDireccion(direccionT.getText().trim())) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce una dirección del empleado valida");
            return;
        }
        empleado.setDireccion(direccionT.getText());
        if(codigopostalT.getText().trim().isEmpty() || codigopostalT.getText().trim().length() != 5 ||
                !Util.esNumero(codigopostalT.getText().trim())) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce un código postal valido");
            return;
        }
        empleado.setCodigopostal(Integer.parseInt(codigopostalT.getText().trim()));
        if(ciudadT.getText().trim().isEmpty()) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce la ciudad del empleado");
            return;
        }
        empleado.setCiudad(ciudadT.getText());
        if(empleado.getIdEmpleado() == null) {
            if(contraseniaT.getText().trim().isEmpty()) {
                Util.dialogo(Alert.AlertType.ERROR, "Introduce la contraseña del empleado");
                return;
            }
            empleado.setContrasenia(contraseniaT.getText());
            if(crearEmpleado(empleado)) {
                Util.dialogo(Alert.AlertType.INFORMATION, "Empleado creado correctamente");
                actualizarEmpleados();
                limpiarFormulario();
            } else {
                Util.dialogo(Alert.AlertType.ERROR, "Ocurrio un error al crear el empleado");
            }
        } else {
            if(editarEmpleado(empleado)) {
                Util.dialogo(Alert.AlertType.INFORMATION, "Empleado editado correctamente");
                actualizarEmpleados();
                limpiarFormulario();
            } else {
                Util.dialogo(Alert.AlertType.ERROR, "Ocurrio un error al editar el empleado");
            }
        }
    }
    
    private boolean crearEmpleado(Empleado empleado) {
        SqlSession conn = MyBatisUtil.getSession();
        int result = 0;
        try {
            EmpleadoDAO empleadoDAO = conn.getMapper(EmpleadoDAO.class);
            if(empleadoDAO.existeEmpleado(empleado.getTelefono(), empleado.getEmail()).isEmpty()) {
                result = empleadoDAO.crearEmpleado(empleado);
            } else {
                Util.dialogo(Alert.AlertType.ERROR, "Ya existe un empleado en el sistema con el mismo telefono y/o email");
            }
            conn.commit();
        } catch(Exception e) {
            Util.excepcion(e);
            conn.rollback();
        } finally {
            conn.close();
        }
        return result > 0;
    }
    
    private boolean editarEmpleado(Empleado empleado) {
        SqlSession conn = MyBatisUtil.getSession();
        int result = 0;
        try {
            EmpleadoDAO empleadoDAO = conn.getMapper(EmpleadoDAO.class);
            if(!empleado.getTelefono().equals(telefonoOriginal)) {
                if(!empleadoDAO.existeEmpleado(empleado.getTelefono(), null).isEmpty()) {
                    Util.dialogo(Alert.AlertType.ERROR, "Ya existe un empleado en el sistema con el mismo telefono");
                    limpiarFormulario();
                    actualizarEmpleados();
                    return false;
                }
            }
            if(!empleado.getEmail().equals(emailOriginal)) {
                if(!empleadoDAO.existeEmpleado(null, empleado.getEmail()).isEmpty()) {
                    Util.dialogo(Alert.AlertType.ERROR, "Ya existe un empleado en el sistema con el mismo email");
                    limpiarFormulario();
                    actualizarEmpleados();
                    return false;
                }
            }
            result = empleadoDAO.editarEmpleado(empleado);
            conn.commit();
        } catch (Exception e) {
            Util.excepcion(e);
            conn.rollback();
        } finally {
            conn.close();
        }
        return result > 0;
    }
    
    @FXML
    private void eliminarEmpleado() {
        if(empleado == null || empleado.getIdEmpleado() == null) {
            Util.dialogo(Alert.AlertType.ERROR, "Selecciona un empleado");
            return;
        }
        if(empleadoLogin != null && Objects.equals(empleado.getIdEmpleado(), empleadoLogin.getIdEmpleado())) {
            Util.dialogo(Alert.AlertType.ERROR, "No se puede eliminar un empleado con la sesión activa");
            return;
        }
        Optional<ButtonType> opcion = Util.confirmacion("Eliminar", "¿Desea eliminar el empleado " + empleado.getNombres() + " " + empleado.getApellidos() + "?");
        if(opcion.get() == ButtonType.YES) {
            if(eliminarEmpleado(empleado.getIdEmpleado())) {
                Util.dialogo(Alert.AlertType.INFORMATION, "Empleado eliminado correctamente");
                actualizarEmpleados();
                limpiarFormulario();
            } else {
                Util.dialogo(Alert.AlertType.ERROR, "Ocurrio un error al eliminar el empleado");
            }
        }
    }
    
    private boolean eliminarEmpleado(Integer idEmpleado) {
        SqlSession conn = MyBatisUtil.getSession();
        int result = 0;
        try {
            EmpleadoDAO empleadoDAO = conn.getMapper(EmpleadoDAO.class);
            result = empleadoDAO.eliminarEmpleado(idEmpleado);
            conn.commit();
        } catch (Exception e) {
            Util.excepcion(e);
            conn.rollback();
        } finally {
            conn.close();
        }
        return result > 0;
    }
    
    private List obtenerEmpleados() {
        List<Empleado> list = null;
        SqlSession conn = MyBatisUtil.getSession();
        try {
            EmpleadoDAO empleadoDAO = conn.getMapper(EmpleadoDAO.class);
            list = empleadoDAO.obtenerEmpleados();
        } catch (Exception e) {
            Util.excepcion(e);
        } finally {
            conn.close();
        }
        return list;
    }
    
    private List obtenerEmpleados(String busqueda) {
        List<Empleado> list = null;
        SqlSession conn = MyBatisUtil.getSession();
        try {
            EmpleadoDAO empleadoDAO = conn.getMapper(EmpleadoDAO.class);
            list = empleadoDAO.obtenerEmpleadosBusqueda("%"+busqueda+"%");
        } catch (Exception e) {
            Util.excepcion(e);
        } finally {
            conn.close();
        }
        return list;
    }
    
    private boolean cambiarContrasenia(Empleado empleado) {
        SqlSession conn = MyBatisUtil.getSession();
        int result = 0;
        try {
            EmpleadoDAO empleadoDAO = conn.getMapper(EmpleadoDAO.class);
            result = empleadoDAO.cambiarContrasenia(empleado);
            conn.commit();
        } catch (Exception e) {
            Util.excepcion(e);
            conn.rollback();
        } finally {
            conn.close();
        }
        return result > 0;
    }
    
    @FXML
    private void cambiarContrasenia() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Cambiar contraseña");
        dialog.setHeaderText(null);
        dialog.setContentText("Nueva contraseña:");
        Optional<String> result = dialog.showAndWait();
        if(result.isPresent()) {
            if(result.get().trim().isEmpty()) {
                Util.dialogo(Alert.AlertType.ERROR, "Introduce una contraseña valida");
                return;
            }
            empleado.setContrasenia(result.get());
            if(cambiarContrasenia(empleado)) {
                Util.dialogo(Alert.AlertType.INFORMATION, "Contraseña cambiada correctamente");
                actualizarEmpleados();
                limpiarFormulario();
            } else {
                Util.dialogo(Alert.AlertType.ERROR, "Ocurrio un error al intentar cambiar la contraseña");
            }
        }
    }
    
    @FXML
    private void menu() {
        Util.menu(getStage(), getEmpleadoLogin());
    }
}
