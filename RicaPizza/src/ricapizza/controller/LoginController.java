package ricapizza.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import mybatis.MyBatisUtil;
import mybatis.dao.EmpleadoDAO;
import org.apache.ibatis.session.SqlSession;
import ricapizza.Util;
import ricapizza.model.Empleado;

public class LoginController implements Initializable {
    private Stage stage;
    @FXML
    private TextField user;
    @FXML
    private TextField pass;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    
    @FXML
    private void login() {
        if(user.getText().isEmpty()) {
            Util.dialogo(Alert.AlertType.ERROR, "El campo usuario esta vacio");
            return;
        }
        if(pass.getText().isEmpty()) {
            Util.dialogo(Alert.AlertType.ERROR, "El campo contraseña esta vacio");
            return;
        }
        SqlSession conn = MyBatisUtil.getSession();
        try {
            EmpleadoDAO empleadoDAO = conn.getMapper(EmpleadoDAO.class);
            Empleado empleado = empleadoDAO.obtenerEmpleado(user.getText(), pass.getText());
            if(empleado != null) {
                cargarMenu(empleado);
            } else {
                Util.dialogo(Alert.AlertType.ERROR, "Usuario y/o contraseña invalidos");
            }
        } catch(Exception e) {
            Util.excepcion(e);
        }
    }
    
    private void cargarMenu(Empleado empleado) throws Exception {
        Util.menu(getStage(), empleado);
    }
    
    @FXML
    private void salir() {
        Platform.exit();
    }
    
    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}