package ricapizza.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import mybatis.MyBatisUtil;
import mybatis.dao.DetallePedidoDAO;
import mybatis.dao.PedidoDAO;
import mybatis.dao.ProductoDAO;
import org.apache.ibatis.session.SqlSession;
import ricapizza.Util;
import ricapizza.model.DetallePedido;
import ricapizza.model.Empleado;
import ricapizza.model.Pedido;
import ricapizza.model.Producto;

public class DetallePedidoController implements Initializable {
    private Stage stage;
    private Empleado empleado;
    private Pedido pedido;
    private DetallePedido detallePedido;
    private Boolean nuevo;
    private Double precioOriginal;
    private Integer cantidadOriginal;
    
    @FXML
    private TableView<DetallePedido> detallesPedido;
    @FXML
    private TableColumn<DetallePedido, String> codigo;
    @FXML
    private TableColumn<DetallePedido, Number> cantidad;
    @FXML
    private TableColumn<DetallePedido, String> precio;
    @FXML
    private TableColumn<DetallePedido, String> subtotal;
    @FXML
    private ComboBox<Producto> productoC;
    @FXML
    private ComboBox<Integer> cantidadC;
    @FXML
    private Label precioL;
    @FXML
    private Label subtotalL;
    @FXML
    private Label pedidoL;
    @FXML
    private Button guardarB;
    @FXML
    private Button descartarB;
    @FXML
    private Button agregarB;
    @FXML
    private Button eliminarB;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        codigo.setCellValueFactory(data -> data.getValue().getCodigoProperty());
        cantidad.setCellValueFactory(data -> data.getValue().getCantidadPoperty());
        precio.setCellValueFactory(data -> data.getValue().getPrecioUnitarioProperty());
        subtotal.setCellValueFactory(data -> data.getValue().getSubtotalProperty());
        detallesPedido.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null) {
                detallePedido = newValue;
                nuevo = false;
                llenarFormulario();
            }
        });
        productoC.setDisable(true);
        productoC.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null) {
                if(newValue.getInventario() > 0) {
                    llenarCantidad(newValue.getInventario());
                } else {
                    Util.dialogo(Alert.AlertType.ERROR, "El producto seleccionado no esta disponible");
                }
            }
        });
        cantidadC.setDisable(true);
        cantidadC.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null) {
                actualizarFormulario();
            }
        });
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
        actualizarDetallesPedido();
        pedidoL.setText(pedidoL.getText()+getPedido().getIdPedido().toString());
        if(!getPedido().getEstado().equals("En proceso")) {
            guardarB.setDisable(true);
            descartarB.setDisable(true);
            agregarB.setDisable(true);
            eliminarB.setDisable(true);
        }
    }
    
    private void actualizarFormulario() {
        precioL.setText("$"+productoC.getValue().getPrecio().toString());
        subtotalL.setText("$"+Double.toString(productoC.getValue().getPrecio()*cantidadC.getValue()));
    }
     
    private void llenarFormulario() {
        cargarProductos();
        precioL.setText(detallePedido.getPrecioUnitario().toString());
        subtotalL.setText(detallePedido.getSubtotal());
    }
    
    @FXML
    private void limpiarFormulario() {
        nuevo = null;
        detallesPedido.getSelectionModel().clearSelection();
        detallePedido = null;
        productoC.getItems().clear();
        cantidadC.getItems().clear();
        precioL.setText("");
        subtotalL.setText("");
    }
    
    private void actualizarDetallesPedido() {
        detallesPedido.getItems().clear();
        detallesPedido.setItems(FXCollections.observableArrayList(obtenerDetallesPedido()));
    }
    
    @FXML
    private void nuevoDetallePedido() {
        limpiarFormulario();
        nuevo = true;
        detallesPedido.getSelectionModel().clearSelection();
        detallePedido = new DetallePedido(getPedido().getIdPedido(), null, null, null);
        cargarProductos();
    }
    
    @FXML
    private void guardarDetallePedido() {
        if(detallePedido == null) {
            Util.dialogo(Alert.AlertType.ERROR, "Selecciona o agrega un producto antes de continuar");
            return;
        }
        if(productoC.isDisable() || productoC.getValue() == null) {
            Util.dialogo(Alert.AlertType.ERROR, "Selecciona el producto a agregar");
            return;
        }
        detallePedido.setCodigo(productoC.getValue().getCodigo());
        if(!nuevo) {
            precioOriginal = detallePedido.getPrecioUnitario();
        }
        detallePedido.setPrecioUnitario(productoC.getValue().getPrecio());
        if(cantidadC.isDisable() || cantidadC.getValue() == null) {
            Util.dialogo(Alert.AlertType.ERROR, "Selecciona la cantidad del producto deseado");
            return;
        }
        if(!nuevo) {
            cantidadOriginal = detallePedido.getCantidad();
        }
        detallePedido.setCantidad(cantidadC.getValue());
        if(nuevo) {
            if(crearDetallePedido(detallePedido)) {
                Util.dialogo(Alert.AlertType.INFORMATION, "Producto agregado correctamente");
                actualizarDetallesPedido();
                limpiarFormulario();
            } else {
                Util.dialogo(Alert.AlertType.ERROR, "Ocurrio un error al agregar el producto");
            }
        } else {
            if(actualizarDetallePedido(detallePedido)) {
                Util.dialogo(Alert.AlertType.INFORMATION, "Producto editado correctamente");
                actualizarDetallesPedido();
                limpiarFormulario();
            } else {
                Util.dialogo(Alert.AlertType.ERROR, "Ocurrio un error al editar el producto");
            }
        }
    }
    
    private void cargarProductos() {
        List<Producto> productos = obtenerProductosPedido();
        if(nuevo != null && !nuevo) {
            productos = obtenerProductos();
        }
        if(productos != null && !productos.isEmpty()) {
            productoC.setItems(FXCollections.observableArrayList(productos));
            if(nuevo != null && !nuevo) {
                Producto producto = obtenerProducto(detallePedido.getCodigo());
                if(producto != null) {
                    productoC.getSelectionModel().select(producto);
                    llenarCantidad(producto.getInventario()+detallePedido.getCantidad());
                }
            }
            if(getPedido().getEstado().equals("En proceso")) {
                productoC.setDisable(false);
            }
        } else {
            Util.dialogo(Alert.AlertType.ERROR, "No hay productos en el sistema");
        }
    }
    
    private void llenarCantidad(Integer cantidad) {
        List<Integer> cantidades = new ArrayList<>();
        for(Integer i = 1; i <= cantidad; i++) {
            cantidades.add(i);
        }
        cantidadC.setItems(FXCollections.observableArrayList(cantidades));
        if(nuevo != null && !nuevo) {
            cantidadC.getSelectionModel().select(detallePedido.getCantidad());
        }
        if(getPedido().getEstado().equals("En proceso")) {
            cantidadC.setDisable(false);
        }
    }
    
    private Producto obtenerProducto(String codigo) {
        Producto producto = null;
        SqlSession conn = MyBatisUtil.getSession();
        try {
            ProductoDAO productoDAO = conn.getMapper(ProductoDAO.class);
            producto = productoDAO.obtenerProducto(codigo);
        } catch (Exception e) {
            Util.excepcion(e);
        } finally {
            conn.close();
        }
        return producto;
    }
    
    private List obtenerProductos() {
        List<Producto> list = null;
        SqlSession conn = MyBatisUtil.getSession();
        try {
            ProductoDAO productoDAO = conn.getMapper(ProductoDAO.class);
            list = productoDAO.obtenerProductos();
        } catch (Exception e) {
            Util.excepcion(e);
        } finally {
            conn.close();
        }
        return list;
    }
    
    private List obtenerProductosPedido() {
        List<Producto> list = null;
        SqlSession conn = MyBatisUtil.getSession();
        try {
            ProductoDAO productoDAO = conn.getMapper(ProductoDAO.class);
            list = productoDAO.obtenerProductosPedido(getPedido().getIdPedido());
        } catch (Exception e) {
            Util.excepcion(e);
        } finally {
            conn.close();
        }
        return list;
    }
    
    private boolean crearDetallePedido(DetallePedido detallePedido) {
        SqlSession conn = MyBatisUtil.getSession();
        int result = 0;
        try {
            DetallePedidoDAO detallePedidoDAO = conn.getMapper(DetallePedidoDAO.class);
            PedidoDAO pedidoDAO = conn.getMapper(PedidoDAO.class);
            ProductoDAO productoDAO = conn.getMapper(ProductoDAO.class);
            if(detallePedidoDAO.obtenerDetallePedido(detallePedido.getIdPedido(), detallePedido.getCodigo()).isEmpty()) {
                getPedido().setNumProductos(getPedido().getNumProductos()+detallePedido.getCantidad());
                getPedido().setPrecioTotal(getPedido().getPrecioTotal()+(detallePedido.getCantidad()*detallePedido.getPrecioUnitario()));
                pedidoDAO.actualizarPedido(getPedido());
                Producto producto = productoC.getValue();
                producto.setInventario(producto.getInventario()-detallePedido.getCantidad());
                productoDAO.editarProducto(producto);
                result = detallePedidoDAO.crearDetallePedido(detallePedido);
            } else {
                Util.dialogo(Alert.AlertType.ERROR, "Ya existe un producto en el sistema para el mismo pedido");
            }
            conn.commit();
        } catch(Exception e) {
            Util.excepcion(e);
            conn.rollback();
        } finally {
            conn.close();
        }
        return result > 0;
    }
    
    private boolean actualizarDetallePedido(DetallePedido detallePedido) {
        SqlSession conn = MyBatisUtil.getSession();
        int result = 0;
        try {
            DetallePedidoDAO detallePedidoDAO = conn.getMapper(DetallePedidoDAO.class);
            PedidoDAO pedidoDAO = conn.getMapper(PedidoDAO.class);
            ProductoDAO productoDAO = conn.getMapper(ProductoDAO.class);
            getPedido().setNumProductos(getPedido().getNumProductos()-cantidadOriginal);
            getPedido().setPrecioTotal(getPedido().getPrecioTotal()-(cantidadOriginal*precioOriginal));
            getPedido().setNumProductos(getPedido().getNumProductos()+detallePedido.getCantidad());
            getPedido().setPrecioTotal(getPedido().getPrecioTotal()+(detallePedido.getCantidad()*detallePedido.getPrecioUnitario()));
            pedidoDAO.actualizarPedido(getPedido());
            Producto producto = productoC.getValue();
            producto.setInventario(producto.getInventario()+cantidadOriginal);
            producto.setInventario(producto.getInventario()-detallePedido.getCantidad());
            productoDAO.editarProducto(producto);
            result = detallePedidoDAO.actualizarDetallePedido(detallePedido);
            conn.commit();
        } catch (Exception e) {
            Util.excepcion(e);
            conn.rollback();
        } finally {
            conn.close();
        }
        return result > 0;
    }
    
    @FXML
    private void eliminarDetallePedido() {
        if(detallePedido == null) {
            Util.dialogo(Alert.AlertType.ERROR, "Selecciona un producto");
            return;
        }
        Optional<ButtonType> opcion = Util.confirmacion("Eliminar", "¿Desea eliminar el producto " + detallePedido.getCodigo()+ "?");
        if(opcion.get() == ButtonType.YES) {
            if(eliminarDetallePedido(detallePedido.getCodigo())) {
                Util.dialogo(Alert.AlertType.INFORMATION, "Producto eliminado correctamente");
                actualizarDetallesPedido();
                limpiarFormulario();
            } else {
                Util.dialogo(Alert.AlertType.ERROR, "Ocurrio un error al eliminar el producto");
            }
        }
    }
    
    private boolean eliminarDetallePedido(String codigo) {
        SqlSession conn = MyBatisUtil.getSession();
        int result = 0;
        try {
            DetallePedidoDAO detallePedidoDAO = conn.getMapper(DetallePedidoDAO.class);
            PedidoDAO pedidoDAO = conn.getMapper(PedidoDAO.class);
            ProductoDAO productoDAO = conn.getMapper(ProductoDAO.class);
            getPedido().setNumProductos(getPedido().getNumProductos()-detallePedido.getCantidad());
            getPedido().setPrecioTotal(getPedido().getPrecioTotal()-(detallePedido.getCantidad()*detallePedido.getPrecioUnitario()));
            pedidoDAO.actualizarPedido(getPedido());
            Producto producto = productoC.getValue();
            producto.setInventario(producto.getInventario()+detallePedido.getCantidad());
            productoDAO.editarProducto(producto);
            result = detallePedidoDAO.eliminarDetallePedido(getPedido().getIdPedido(), codigo);
            conn.commit();
        } catch (Exception e) {
            Util.excepcion(e);
            conn.rollback();
        } finally {
            conn.close();
        }
        return result > 0;
    }
    
    private List obtenerDetallesPedido() {
        List<DetallePedido> list = null;
        SqlSession conn = MyBatisUtil.getSession();
        try {
            DetallePedidoDAO detallePedidoDAO = conn.getMapper(DetallePedidoDAO.class);
            list = detallePedidoDAO.obtenerDetallesPedido(getPedido().getIdPedido());
        } catch (Exception e) {
            Util.excepcion(e);
        } finally {
            conn.close();
        }
        return list;
    }
        
    @FXML
    private void pedidos() {
        Util.pedidos(getStage(), getEmpleado());
    }
}