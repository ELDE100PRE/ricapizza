package ricapizza.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;
import ricapizza.Util;
import ricapizza.model.Empleado;

public class MenuController implements Initializable {
    private Stage stage;
    private Empleado empleado;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    
    @FXML
    private void empleados() {
        Util.empleados(getStage(), getEmpleado());
    }
    
    @FXML
    private void clientes() {
        Util.clientes(getStage(), getEmpleado());
    }
    
    @FXML
    private void productos() {
        Util.productos(getStage(), getEmpleado());
    }
    
    @FXML
    private void pedidos() {
        Util.pedidos(getStage(), getEmpleado());
    }
    
    @FXML
    private void login() {
        Util.login(getStage());
    }
    
    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }
}
