package ricapizza.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;
import mybatis.MyBatisUtil;
import mybatis.dao.ClienteDAO;
import mybatis.dao.DetallePedidoDAO;
import mybatis.dao.PedidoDAO;
import mybatis.dao.ProductoDAO;
import org.apache.ibatis.session.SqlSession;
import ricapizza.Util;
import ricapizza.model.Cliente;
import ricapizza.model.DetallePedido;
import ricapizza.model.Empleado;
import ricapizza.model.Pedido;
import ricapizza.model.Producto;

public class PedidosController implements Initializable {
    private Stage stage;
    private Empleado empleado;
    private Pedido pedido;
    
    @FXML
    private TableView<Pedido> pedidos;
    @FXML
    private TableColumn<Pedido, String> numPedido;
    @FXML
    private TableColumn<Pedido, String> fechaHora;
    @FXML
    private TableColumn<Pedido, String> estado;
    @FXML
    private TableColumn<Pedido, String> numProductos;
    @FXML
    private TableColumn<Pedido, String> total;
    @FXML
    private Label numPedidoL;
    @FXML
    private Label clienteL;
    @FXML
    private Label fechaHoraL;
    @FXML
    private Label estadoL;
    @FXML
    private Label numProductosL;
    @FXML
    private Label totalL;
    @FXML
    private Button cambiarEstadoB;
    @FXML
    private Button editarPedidoB;
    @FXML
    private Button eliminarPedidoB;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        pedidos.setItems(FXCollections.observableArrayList(obtenerPedidos()));
        numPedido.setCellValueFactory(data -> data.getValue().getNumPedidoProperty());
        fechaHora.setCellValueFactory(data -> data.getValue().getFechaHoraProperty());
        estado.setCellValueFactory(data -> data.getValue().getEstadoProperty());
        numProductos.setCellValueFactory(data -> data.getValue().getNumProductosProperty());
        total.setCellValueFactory(data -> data.getValue().getTotalProperty());
        pedidos.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null) {
                pedido = newValue;
                llenarFormulario();
            }
        });
        cambiarEstadoB.managedProperty().bind(cambiarEstadoB.visibleProperty());
        cambiarEstadoB.setVisible(false);
        editarPedidoB.managedProperty().bind(editarPedidoB.visibleProperty());
        editarPedidoB.setVisible(false);
        eliminarPedidoB.managedProperty().bind(eliminarPedidoB.visibleProperty());
        eliminarPedidoB.setVisible(false);
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }
        
    private void llenarFormulario() {
        numPedidoL.setText("#"+pedido.getIdPedido().toString());
        clienteL.setText(obtenerNombreCliente(pedido));
        fechaHoraL.setText(Util.obtenerFechaHora(pedido.getFechaHora()));
        estadoL.setText(pedido.getEstado());
        numProductosL.setText(pedido.getNumProductos().toString()+" producto(s)");
        totalL.setText("$"+pedido.getPrecioTotal().toString());
        cambiarEstadoB.setVisible(false);
        if(pedido.getEstado().equals("En proceso")) {
            cambiarEstadoB.setVisible(true);
            editarPedidoB.setText("Editar");
            editarPedidoB.setVisible(true);
        } else {
            editarPedidoB.setText("Ver");
            editarPedidoB.setVisible(true);
        }
        if(pedido.getEstado().equals("En proceso") && pedido.getNumProductos().equals(0)) {
            cambiarEstadoB.setVisible(false);
            eliminarPedidoB.setVisible(true);
        } else {
            eliminarPedidoB.setVisible(false);
        }
    }
    
    private String obtenerNombreCliente(Pedido pedido) {
        Cliente cliente = null;
        SqlSession conn = MyBatisUtil.getSession();
        try {
            ClienteDAO clienteDAO = conn.getMapper(ClienteDAO.class);
            cliente = clienteDAO.obtenerCliente(pedido.getIdCliente());
        } catch (Exception e) {
            Util.excepcion(e);
        } finally {
            conn.close();
        }
        if(cliente != null) {
            return cliente.toString();
        } else {
            return "";
        }
    }
    
    @FXML
    private void limpiarFormulario() {
        pedidos.getSelectionModel().clearSelection();
        pedido = null;
        numPedidoL.setText("");
        clienteL.setText("");
        fechaHoraL.setText("");
        estadoL.setText("");
        numProductosL.setText("");
        totalL.setText("");
        cambiarEstadoB.setVisible(true);
        editarPedidoB.setText("Editar");
        editarPedidoB.setVisible(true);
        eliminarPedidoB.setVisible(false);
    }
    
    @FXML
    private void cambiarEstado() {
        List<String> estados = new ArrayList<>();
        estados.add("Cancelado");
        estados.add("Entregado");
        ChoiceDialog<String> dialog = new ChoiceDialog<>(null, estados);
        dialog.setTitle("Cambiar estado de pedido #"+pedido.getIdPedido().toString());
        dialog.setHeaderText(null);
        dialog.setContentText("Nuevo estado:");
        Optional<String> result = dialog.showAndWait();
        if(result.isPresent()) {
            pedido.setEstado(result.get());
            if(actualizarPedido(pedido)) {
                if(pedido.getEstado().equals("Cancelado")) {
                    cancelarPedido();
                }
                busquedaTodos();
                llenarFormulario();
            } else {
                Util.dialogo(Alert.AlertType.ERROR, "Ocurrio un error cambiando el estado del pedido");
            }
        }
    }
    
    private void cancelarPedido() {
        SqlSession conn = MyBatisUtil.getSession();
        try {
            DetallePedidoDAO detallePedidoDAO = conn.getMapper(DetallePedidoDAO.class);
            ProductoDAO productoDAO = conn.getMapper(ProductoDAO.class);
            List<DetallePedido> detallePedido = detallePedidoDAO.obtenerDetallesPedido(pedido.getIdPedido());
            for(DetallePedido pedidoDetalle : detallePedido) {
                Producto producto = productoDAO.obtenerProducto(pedidoDetalle.getCodigo());
                producto.setInventario(producto.getInventario()+pedidoDetalle.getCantidad());
                productoDAO.editarProducto(producto);
            }
            conn.commit();
        } catch (Exception e) {
            Util.excepcion(e);
            conn.rollback();
        } finally {
            conn.close();
        }
    }    
    
    @FXML
    private void busquedaCliente() {
        List<Cliente> clientes = obtenerClientes();
        if(clientes != null) {
            ChoiceDialog<Cliente> dialog = new ChoiceDialog<>(null, clientes);
            dialog.setTitle("Buscar pedidos por cliente");
            dialog.setHeaderText(null);
            dialog.setContentText("Cliente:");
            Optional<Cliente> result = dialog.showAndWait();
            if(result.isPresent()) {
                pedidos.getItems().clear();
                pedidos.setItems(FXCollections.observableArrayList(obtenerPedidosCliente(result.get().getIdCliente())));
            }
        } else {
            Util.dialogo(Alert.AlertType.ERROR, "No hay clientes en el sistema");
        }
    }
    
    @FXML
    private void busquedaFecha() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Buscar pedidos por fecha");
        dialog.setHeaderText(null);
        dialog.setContentText("Fecha:");
        Optional<String> result = dialog.showAndWait();
        if(result.isPresent()) {
            if(result.get().trim().isEmpty() || !Util.esFecha(result.get().trim())) {
                Util.dialogo(Alert.AlertType.ERROR, "Introduce una fecha valida");
                return;
            }
            pedidos.getItems().clear();
            pedidos.setItems(FXCollections.observableArrayList(obtenerPedidosFecha(result.get().trim())));
        }
    }
    
    @FXML
    private void busquedaEstado() {
        List<String> estados = new ArrayList<>();
        estados.add("En proceso");
        estados.add("Cancelado");
        estados.add("Entregado");
        ChoiceDialog<String> dialog = new ChoiceDialog<>(null, estados);
        dialog.setTitle("Buscar pedidos por estado");
        dialog.setHeaderText(null);
        dialog.setContentText("Estado:");
        Optional<String> result = dialog.showAndWait();
        if(result.isPresent()) {
            pedidos.getItems().clear();
            pedidos.setItems(FXCollections.observableArrayList(obtenerPedidosEstado(result.get())));
        }
    }
    
    @FXML
    private void busquedaTodos() {
        pedidos.getItems().clear();
        pedidos.setItems(FXCollections.observableArrayList(obtenerPedidos()));
    }
    
    @FXML
    private void nuevoPedido() {
        pedidos.getSelectionModel().clearSelection();
        List<Cliente> clientes = obtenerClientes();
        if(clientes != null) {
            ChoiceDialog<Cliente> dialog = new ChoiceDialog<>(null, clientes);
            dialog.setTitle("Nuevo pedido");
            dialog.setHeaderText(null);
            dialog.setContentText("Cliente:");
            Optional<Cliente> result = dialog.showAndWait();
            if(result.isPresent()) {
                pedido = new Pedido(null, null, "En proceso", 0, 0d, result.get().getIdCliente());
                if(crearPedido(pedido)) {
                    Util.detallepedido(getStage(), getEmpleado(), pedido);
                } else {
                   Util.dialogo(Alert.AlertType.ERROR, "Ocurrio un error al crear el pedido");
                }
            }
        } else {
            Util.dialogo(Alert.AlertType.ERROR, "No hay clientes en el sistema");
        }
    }
    
    @FXML
    private void editarPedido() {
        Util.detallepedido(getStage(), getEmpleado(), pedido);
    }
    
    @FXML
    private void eliminarPedido() {
        Optional<ButtonType> result = Util.confirmacion("Eliminar pedido", "Esta seguro que desea eliminar el pedido #"+pedido.getIdPedido().toString());
        if(result.isPresent()) {
            if(result.get() == ButtonType.YES) {
                if(eliminarPedido(pedido.getIdPedido())) {
                    limpiarFormulario();
                    busquedaTodos();
                } else {
                    Util.dialogo(Alert.AlertType.ERROR, "Ocurrio un error al eliminar el pedido");
                }
            }
        }
    }
    
    private boolean crearPedido(Pedido pedido) {
        SqlSession conn = MyBatisUtil.getSession();
        int result = 0;
        try {
            PedidoDAO pedidoDAO = conn.getMapper(PedidoDAO.class);
            result = pedidoDAO.crearPedido(pedido);
            conn.commit();
        } catch (Exception e) {
            Util.excepcion(e);
            conn.rollback();
        } finally {
            conn.close();
        }
        return result > 0;
    }
    
    private boolean eliminarPedido(Integer idPedido) {
        SqlSession conn = MyBatisUtil.getSession();
        int result = 0;
        try {
            PedidoDAO pedidoDAO = conn.getMapper(PedidoDAO.class);
            result = pedidoDAO.eliminarPedido(idPedido);
            conn.commit();
        } catch (Exception e) {
            Util.excepcion(e);
            conn.rollback();
        } finally {
            conn.close();
        }
        return result > 0;
    }
    
    private boolean actualizarPedido(Pedido pedido) {
        SqlSession conn = MyBatisUtil.getSession();
        int result = 0;
        try {
            PedidoDAO pedidoDAO = conn.getMapper(PedidoDAO.class);
            result = pedidoDAO.actualizarPedido(pedido);
            conn.commit();
        } catch (Exception e) {
            Util.excepcion(e);
            conn.rollback();
        } finally {
            conn.close();
        }
        return result > 0;
    }
    
    private List obtenerClientes() {
        List<Cliente> list = null;
        SqlSession conn = MyBatisUtil.getSession();
        try {
            ClienteDAO clienteDAO = conn.getMapper(ClienteDAO.class);
            list = clienteDAO.obtenerClientes();
        } catch (Exception e) {
            Util.excepcion(e);
        } finally {
            conn.close();
        }
        return list;
    }
    
    private List obtenerPedidosCliente(Integer idCliente) {
        List<Pedido> list = null;
        SqlSession conn = MyBatisUtil.getSession();
        try {
            PedidoDAO pedidoDAO = conn.getMapper(PedidoDAO.class);
            list = pedidoDAO.obtenerPedidosCliente(idCliente);
        } catch (Exception e) {
            Util.excepcion(e);
        } finally {
            conn.close();
        }
        return list;
    }
    
    private List obtenerPedidosFecha(String fecha) {
        List<Pedido> list = null;
        SqlSession conn = MyBatisUtil.getSession();
        try {
            PedidoDAO pedidoDAO = conn.getMapper(PedidoDAO.class);
            list = pedidoDAO.obtenerPedidosFecha(fecha);
        } catch (Exception e) {
            Util.excepcion(e);
        } finally {
            conn.close();
        }
        return list;
    }
    
    private List obtenerPedidosEstado(String estado) {
        List<Pedido> list = null;
        SqlSession conn = MyBatisUtil.getSession();
        try {
            PedidoDAO pedidoDAO = conn.getMapper(PedidoDAO.class);
            list = pedidoDAO.obtenerPedidosEstado(estado);
        } catch (Exception e) {
            Util.excepcion(e);
        } finally {
            conn.close();
        }
        return list;
    }
    
    private List obtenerPedidos() {
        List<Pedido> list = null;
        SqlSession conn = MyBatisUtil.getSession();
        try {
            PedidoDAO pedidoDAO = conn.getMapper(PedidoDAO.class);
            list = pedidoDAO.obtenerPedidos();
        } catch (Exception e) {
            Util.excepcion(e);
        } finally {
            conn.close();
        }
        return list;
    }
        
    @FXML
    private void menu() {
        Util.menu(getStage(), getEmpleado());
    }
}