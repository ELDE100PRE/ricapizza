package ricapizza.controller;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import mybatis.MyBatisUtil;
import mybatis.dao.ClienteDAO;
import mybatis.dao.PedidoDAO;
import org.apache.ibatis.session.SqlSession;
import ricapizza.Util;
import ricapizza.model.Cliente;
import ricapizza.model.Empleado;

public class ClientesController implements Initializable {
    private Stage stage;
    private Empleado empleado;
    private Cliente cliente;
    private String telefonoOriginal;
    private String emailOriginal;
        
    @FXML
    private TableView<Cliente> clientes;
    @FXML
    private TableColumn<Cliente, String> nombres;
    @FXML
    private TableColumn<Cliente, String> apellidos;
    @FXML
    private TableColumn<Cliente, String> telefono;
    @FXML
    private TableColumn<Cliente, String> email;
    @FXML
    private TableColumn<Cliente, String> direccion;
    @FXML
    private TableColumn<Cliente, Number> codigopostal;
    @FXML
    private TableColumn<Cliente, String> ciudad;
    @FXML
    private TextField nombresT;
    @FXML
    private TextField apellidosT;
    @FXML
    private TextField telefonoT;
    @FXML
    private TextField emailT;
    @FXML
    private TextField direccionT;
    @FXML
    private TextField codigopostalT;
    @FXML
    private TextField ciudadT;
    @FXML
    private TextField buscar;
    @FXML
    private GridPane formulario;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        clientes.setItems(FXCollections.observableArrayList(obtenerClientes()));
        nombres.setCellValueFactory(data -> data.getValue().getNombresProperty());
        apellidos.setCellValueFactory(data -> data.getValue().getApellidosProperty());
        telefono.setCellValueFactory(data -> data.getValue().getTelefonoProperty());
        email.setCellValueFactory(data -> data.getValue().getEmailProperty());
        direccion.setCellValueFactory(data -> data.getValue().getDireccionProperty());
        codigopostal.setCellValueFactory(data -> data.getValue().getCodigopostalProperty());
        ciudad.setCellValueFactory(data -> data.getValue().getCiudadProperty());
        clientes.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null) {
                cliente = newValue;
                llenarFormulario();
            }
        });
        buscar.textProperty().addListener((observable, oldValue, newValue) -> {
            clientes.setItems(FXCollections.observableArrayList(obtenerClientes(newValue)));
        });
        buscar.setText("");
        formulario.setDisable(true);
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }
        
    private void llenarFormulario() {
        formulario.setDisable(false);
        nombresT.setText(cliente.getNombres());
        apellidosT.setText(cliente.getApellidos());
        telefonoT.setText(cliente.getTelefono());
        emailT.setText(cliente.getEmail());
        direccionT.setText(cliente.getDireccion());
        codigopostalT.setText(cliente.getCodigopostal().toString());
        ciudadT.setText(cliente.getCiudad());
    }
    
    @FXML
    private void limpiarFormulario() {
        formulario.setDisable(true);
        clientes.getSelectionModel().clearSelection();
        cliente = null;
        nombresT.setText("");
        apellidosT.setText("");
        telefonoT.setText("");
        emailT.setText("");
        direccionT.setText("");
        codigopostalT.setText("");
        ciudadT.setText("");
    }
    
    private void actualizarClientes() {
        clientes.getItems().clear();
        if(buscar.getText().trim().isEmpty()) {
            clientes.setItems(FXCollections.observableArrayList(obtenerClientes()));
        } else {
            clientes.setItems(FXCollections.observableArrayList(obtenerClientes(buscar.getText())));
        }
    }
    
    @FXML
    private void nuevoCliente() {
        clientes.getSelectionModel().clearSelection();
        cliente = new Cliente(null, "", "", "", "", "", null, "");
        llenarFormulario();
        codigopostalT.setText("");
    }
    
    @FXML
    private void guardarCliente() {
        if(cliente == null) {
            Util.dialogo(Alert.AlertType.ERROR, "Selecciona o crea un cliente antes de continuar");
            return;
        }
        if(nombresT.getText().trim().isEmpty()) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce el/los nombre(s) del cliente");
            return;
        }
        cliente.setNombres(nombresT.getText());
        if(apellidosT.getText().trim().isEmpty()) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce el/los apellido(s) del cliente");
            return;
        }
        cliente.setApellidos(apellidosT.getText());
        if(telefonoT.getText().trim().isEmpty() || telefonoT.getText().trim().length() != 10 ||
                !Util.esNumero(telefonoT.getText().trim())) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce un télefono del cliente valido");
            return;
        }
        if(cliente.getIdCliente()!= null) {
            telefonoOriginal = cliente.getTelefono();
        }
        cliente.setTelefono(telefonoT.getText().trim());
        if(emailT.getText().trim().isEmpty() || !Util.esCorreo(emailT.getText().trim())) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce un email del cliente valido");
            return;
        }
        if(cliente.getIdCliente()!= null) {
            emailOriginal = cliente.getEmail();
        }
        cliente.setEmail(emailT.getText().trim());
        if(direccionT.getText().trim().isEmpty() || !Util.esDireccion(direccionT.getText().trim())) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce una dirección del cliente valida");
            return;
        }
        cliente.setDireccion(direccionT.getText());
        if(codigopostalT.getText().trim().isEmpty() || codigopostalT.getText().trim().length() != 5 ||
                !Util.esNumero(codigopostalT.getText().trim())) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce un código postal valido");
            return;
        }
        cliente.setCodigopostal(Integer.parseInt(codigopostalT.getText().trim()));
        if(ciudadT.getText().trim().isEmpty()) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce la ciudad del cliente");
            return;
        }
        cliente.setCiudad(ciudadT.getText());
        if(cliente.getIdCliente() == null) {
            if(crearCliente(cliente)) {
                Util.dialogo(Alert.AlertType.INFORMATION, "Cliente creado correctamente");
                actualizarClientes();
                limpiarFormulario();
            } else {
                Util.dialogo(Alert.AlertType.ERROR, "Ocurrio un error al crear el cliente");
            }
        } else {
            if(editarCliente(cliente)) {
                Util.dialogo(Alert.AlertType.INFORMATION, "Cliente editado correctamente");
                actualizarClientes();
                limpiarFormulario();
            } else {
                Util.dialogo(Alert.AlertType.ERROR, "Ocurrio un error al editar el cliente");
            }
        }
    }
    
    private boolean crearCliente(Cliente cliente) {
        SqlSession conn = MyBatisUtil.getSession();
        int result = 0;
        try {
            ClienteDAO clienteDAO = conn.getMapper(ClienteDAO.class);
            if(clienteDAO.existeCliente(cliente.getTelefono(), cliente.getEmail()).isEmpty()) {
                result = clienteDAO.crearCliente(cliente);
            } else {
                Util.dialogo(Alert.AlertType.ERROR, "Ya existe un cliente en el sistema con el mismo telefono y/o email");
            }
            conn.commit();
        } catch(Exception e) {
            Util.excepcion(e);
            conn.rollback();
        } finally {
            conn.close();
        }
        return result > 0;
    }
    
    private boolean editarCliente(Cliente cliente) {
        SqlSession conn = MyBatisUtil.getSession();
        int result = 0;
        try {
            ClienteDAO clienteDAO = conn.getMapper(ClienteDAO.class);
            if(!cliente.getTelefono().equals(telefonoOriginal)) {
                if(!clienteDAO.existeCliente(cliente.getTelefono(), null).isEmpty()) {
                    Util.dialogo(Alert.AlertType.ERROR, "Ya existe un cliente en el sistema con el mismo telefono");
                    limpiarFormulario();
                    actualizarClientes();
                    return false;
                }
            }
            if(!cliente.getEmail().equals(emailOriginal)) {
                if(!clienteDAO.existeCliente(null, cliente.getEmail()).isEmpty()) {
                    Util.dialogo(Alert.AlertType.ERROR, "Ya existe un cliente en el sistema con el mismo email");
                    limpiarFormulario();
                    actualizarClientes();
                    return false;
                }
            }
            result = clienteDAO.editarCliente(cliente);
            conn.commit();
        } catch (Exception e) {
            Util.excepcion(e);
            conn.rollback();
        } finally {
            conn.close();
        }
        return result > 0;
    }
    
    @FXML
    private void eliminarCliente() {
        if(cliente == null || cliente.getIdCliente() == null) {
            Util.dialogo(Alert.AlertType.ERROR, "Selecciona un cliente");
            return;
        }
        if(existenPedidos()) {
            Util.dialogo(Alert.AlertType.ERROR, "No se puede eliminar un cliente con pedidos en el sistema");
            return;
        }
        Optional<ButtonType> opcion = Util.confirmacion("Eliminar", "¿Desea eliminar el cliente " + cliente.getNombres() + " " + cliente.getApellidos() + "?");
        if(opcion.get() == ButtonType.YES) {
            if(eliminarCliente(cliente.getIdCliente())) {
                Util.dialogo(Alert.AlertType.INFORMATION, "Cliente eliminado correctamente");
                actualizarClientes();
                limpiarFormulario();
            } else {
                Util.dialogo(Alert.AlertType.ERROR, "Ocurrio un error al eliminar el cliente");
            }
        }
    }
    
    private boolean existenPedidos() {
        SqlSession conn = MyBatisUtil.getSession();
        try {
            PedidoDAO pedidoDAO = conn.getMapper(PedidoDAO.class);
            return !pedidoDAO.obtenerPedidosCliente(cliente.getIdCliente()).isEmpty();
        } catch (Exception e) {
            Util.excepcion(e);
        } finally {
            conn.close();
        }
        return false;
    }
    
    private boolean eliminarCliente(Integer idCliente) {
        SqlSession conn = MyBatisUtil.getSession();
        int result = 0;
        try {
            ClienteDAO clienteDAO = conn.getMapper(ClienteDAO.class);
            result = clienteDAO.eliminarCliente(idCliente);
            conn.commit();
        } catch (Exception e) {
            Util.excepcion(e);
            conn.rollback();
        } finally {
            conn.close();
        }
        return result > 0;
    }
    
    private List obtenerClientes() {
        List<Cliente> list = null;
        SqlSession conn = MyBatisUtil.getSession();
        try {
            ClienteDAO clienteDAO = conn.getMapper(ClienteDAO.class);
            list = clienteDAO.obtenerClientes();
        } catch (Exception e) {
            Util.excepcion(e);
        } finally {
            conn.close();
        }
        return list;
    }
    
    private List obtenerClientes(String busqueda) {
        List<Cliente> list = null;
        SqlSession conn = MyBatisUtil.getSession();
        try {
            ClienteDAO clienteDAO = conn.getMapper(ClienteDAO.class);
            list = clienteDAO.obtenerClientesBusqueda("%"+busqueda+"%");
        } catch (Exception e) {
            Util.excepcion(e);
        } finally {
            conn.close();
        }
        return list;
    }
        
    @FXML
    private void menu() {
        Util.menu(getStage(), getEmpleado());
    }
}