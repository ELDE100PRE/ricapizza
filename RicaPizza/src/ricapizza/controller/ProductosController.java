package ricapizza.controller;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import mybatis.MyBatisUtil;
import mybatis.dao.DetallePedidoDAO;
import mybatis.dao.ProductoDAO;
import org.apache.ibatis.session.SqlSession;
import ricapizza.Util;
import ricapizza.model.Empleado;
import ricapizza.model.Producto;

public class ProductosController implements Initializable {
    private Stage stage;
    private Empleado empleado;
    private Producto producto;
    private Boolean nuevoProducto;
    
    @FXML
    private TableView<Producto> productos;
    @FXML
    private TableColumn<Producto, String> codigo;
    @FXML
    private TableColumn<Producto, String> nombre;
    @FXML
    private TableColumn<Producto, Number> precio;
    @FXML
    private TableColumn<Producto, Number> inventario;
    @FXML
    private TextField codigoT;
    @FXML
    private TextField cantidadT;
    @FXML
    private TextField nombreT;
    @FXML
    private TextField descripcionT;
    @FXML
    private TextField precioT;
    @FXML
    private TextField restriccionesT;
    @FXML
    private TextField inventarioT;
    @FXML
    private TextField buscar;
    @FXML
    private GridPane formulario;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        productos.setItems(FXCollections.observableArrayList(obtenerProductos()));
        codigo.setCellValueFactory(data -> data.getValue().getCodigoProperty());
        nombre.setCellValueFactory(data -> data.getValue().getNombreProperty());
        precio.setCellValueFactory(data -> data.getValue().getPrecioProperty());
        inventario.setCellValueFactory(data -> data.getValue().getInventarioProperty());
        productos.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null) {
                producto = newValue;
                nuevoProducto = false;
                llenarFormulario();
            }
        });
        buscar.textProperty().addListener((observable, oldValue, newValue) -> {
            productos.setItems(FXCollections.observableArrayList(obtenerProductos(newValue)));
        });
        buscar.setText("");
        formulario.setDisable(true);
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }
        
    private void llenarFormulario() {
        formulario.setDisable(false);
        codigoT.setDisable(true);
        codigoT.setText(producto.getCodigo());
        if(producto.getCantidad() == null) {
            cantidadT.setText("");
        } else {
            cantidadT.setText(producto.getCantidad().toString());
        }
        nombreT.setText(producto.getNombre());
        descripcionT.setText(producto.getDescripcion());
        precioT.setText(producto.getPrecio().toString());
        if(producto.getRestricciones() == null) {
            restriccionesT.setText("");
        } else {
            restriccionesT.setText(producto.getRestricciones());
        }
        inventarioT.setText(producto.getInventario().toString());
    }
    
    @FXML
    private void limpiarFormulario() {
        formulario.setDisable(true);
        productos.getSelectionModel().clearSelection();
        producto = null;
        nuevoProducto = null;
        codigoT.setDisable(false);
        codigoT.setText("");
        cantidadT.setText("");
        nombreT.setText("");
        descripcionT.setText("");
        precioT.setText("");
        restriccionesT.setText("");
        inventarioT.setText("");
    }
    
    private void actualizarProductos() {
        productos.getItems().clear();
        if(buscar.getText().trim().isEmpty()) {
            productos.setItems(FXCollections.observableArrayList(obtenerProductos()));
        } else {
            productos.setItems(FXCollections.observableArrayList(obtenerProductos(buscar.getText())));
        }
    }
    
    @FXML
    private void nuevoProducto() {
        productos.getSelectionModel().clearSelection();
        producto = new Producto("", null, "", "", 0d, null, 1);
        nuevoProducto = true;
        llenarFormulario();
        codigoT.setDisable(false);
        restriccionesT.setText("");
    }
    
    @FXML
    private void guardarProducto() {
        if(producto == null) {
            Util.dialogo(Alert.AlertType.ERROR, "Selecciona o crea un producto antes de continuar");
            return;
        }
        if(codigoT.getText().trim().isEmpty() || codigoT.getText().trim().length() > 10) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce un código del producto de un maximo de 10 caracteres");
            return;
        }
        producto.setCodigo(codigoT.getText());
        if(!cantidadT.getText().trim().isEmpty() && !Util.esNumeroDecimal(cantidadT.getText().trim())) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce una cantidad con decimales valida");
            return;
        }
        if(cantidadT.getText().trim().isEmpty()) {
            producto.setCantidad(null);
        } else {
            producto.setCantidad(Double.parseDouble(cantidadT.getText()));
        }
        if(nombreT.getText().trim().isEmpty()) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce un nombre para el producto");
            return;
        }
        producto.setNombre(nombreT.getText());
        if(descripcionT.getText().trim().isEmpty()) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce una descripción para el producto");
            return;
        } 
        producto.setDescripcion(descripcionT.getText());
        if(precioT.getText().trim().isEmpty()) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce el precio del producto");
            return;
        }
        if(!precioT.getText().trim().isEmpty() && !Util.esNumeroDecimal(precioT.getText().trim())) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce un precio con decimales valido");
            return;
        }
        producto.setPrecio(Double.parseDouble(precioT.getText()));
        if(restriccionesT.getText().isEmpty()) {
            producto.setRestricciones(null);
        } else { 
            producto.setRestricciones(restriccionesT.getText());
        }
        if(inventarioT.getText().trim().isEmpty()) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce la cantidad del producto disponible en el inventario");
            return;
        }
        if(!inventarioT.getText().trim().isEmpty() && !Util.esNumero(inventarioT.getText().trim())) {
            Util.dialogo(Alert.AlertType.ERROR, "Introduce una cantidad valida del producto disponible en el inventario");
            return;
        }
        producto.setInventario(Integer.parseInt(inventarioT.getText()));
        if(nuevoProducto) {
            if(crearProducto(producto)) {
                Util.dialogo(Alert.AlertType.INFORMATION, "Producto creado correctamente");
                actualizarProductos();
                limpiarFormulario();
            } else {
                Util.dialogo(Alert.AlertType.ERROR, "Ocurrio un error al crear el producto");
            }
        } else {
            if(editarProducto(producto)) {
                Util.dialogo(Alert.AlertType.INFORMATION, "Producto editado correctamente");
                actualizarProductos();
                limpiarFormulario();
            } else {
                Util.dialogo(Alert.AlertType.ERROR, "Ocurrio un error al editar el producto");
            }
        }
    }
    
    private boolean crearProducto(Producto producto) {
        SqlSession conn = MyBatisUtil.getSession();
        int result = 0;
        try {
            ProductoDAO productoDAO = conn.getMapper(ProductoDAO.class);
            if(productoDAO.existeProducto(producto.getCodigo()).isEmpty()) {
                result = productoDAO.crearProducto(producto);
            } else {
                Util.dialogo(Alert.AlertType.ERROR, "Ya existe un producto en el sistema con el mismo código");
            }
            conn.commit();
        } catch(Exception e) {
            Util.excepcion(e);
            conn.rollback();
        } finally {
            conn.close();
        }
        return result > 0;
    }
    
    private boolean editarProducto(Producto producto) {
        SqlSession conn = MyBatisUtil.getSession();
        int result = 0;
        try {
            ProductoDAO productoDAO = conn.getMapper(ProductoDAO.class);
            result = productoDAO.editarProducto(producto);
            conn.commit();
        } catch (Exception e) {
            Util.excepcion(e);
            conn.rollback();
        } finally {
            conn.close();
        }
        return result > 0;
    }
    
    @FXML
    private void eliminarProducto() {
        if(producto == null || producto.getCodigo().isEmpty()) {
            Util.dialogo(Alert.AlertType.ERROR, "Selecciona un producto");
            return;
        }
        if(existenPedidos()) {
            Util.dialogo(Alert.AlertType.ERROR, "No se puede eliminar un producto con pedidos en el sistema");
            return;
        }
        Optional<ButtonType> opcion = Util.confirmacion("Eliminar", "¿Desea eliminar el producto " + producto.getNombre() + "?");
        if(opcion.get() == ButtonType.YES) {
            if(eliminarProducto(producto.getCodigo())) {
                Util.dialogo(Alert.AlertType.INFORMATION, "Producto eliminado correctamente");
                actualizarProductos();
                limpiarFormulario();
            } else {
                Util.dialogo(Alert.AlertType.ERROR, "Ocurrio un error al eliminar el producto");
            }
        }
    }
    
    private boolean existenPedidos() {
        SqlSession conn = MyBatisUtil.getSession();
        try {
            DetallePedidoDAO detallePedidoDAO = conn.getMapper(DetallePedidoDAO.class);
            return !detallePedidoDAO.existeProducto(producto.getCodigo()).isEmpty();
        } catch (Exception e) {
            Util.excepcion(e);
        } finally {
            conn.close();
        }
        return false;
    }
    
    private boolean eliminarProducto(String codigo) {
        SqlSession conn = MyBatisUtil.getSession();
        int result = 0;
        try {
            ProductoDAO productoDAO = conn.getMapper(ProductoDAO.class);
            result = productoDAO.eliminarProducto(codigo);
            conn.commit();
        } catch (Exception e) {
            Util.excepcion(e);
            conn.rollback();
        } finally {
            conn.close();
        }
        return result > 0;
    }
    
    private List obtenerProductos() {
        List<Producto> list = null;
        SqlSession conn = MyBatisUtil.getSession();
        try {
            ProductoDAO productoDAO = conn.getMapper(ProductoDAO.class);
            list = productoDAO.obtenerProductos();
        } catch (Exception e) {
            Util.excepcion(e);
        } finally {
            conn.close();
        }
        return list;
    }
    
    private List obtenerProductos(String busqueda) {
        List<Producto> list = null;
        SqlSession conn = MyBatisUtil.getSession();
        try {
            ProductoDAO productoDAO = conn.getMapper(ProductoDAO.class);
            list = productoDAO.obtenerProductosBusqueda("%"+busqueda+"%");
        } catch (Exception e) {
            Util.excepcion(e);
        } finally {
            conn.close();
        }
        return list;
    }
        
    @FXML
    private void menu() {
        Util.menu(getStage(), getEmpleado());
    }
}