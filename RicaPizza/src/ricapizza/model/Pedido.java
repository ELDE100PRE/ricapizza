/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ricapizza.model;

import java.util.Date;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import ricapizza.Util;

/**
 *
 * @author Carlos
 */
public class Pedido {
    private Integer idPedido;
    private Date fechaHora;
    private String estado;
    private Integer numProductos;
    private Double precioTotal;
    private Integer idCliente;

    public Pedido() {
    }

    public Pedido(Integer idPedido, Date fechaHora, String estado, Integer numProductos, Double precioTotal, Integer idCliente) {
        this.idPedido = idPedido;
        if(fechaHora != null) {
            this.fechaHora = new Date(fechaHora.getTime());
        } else {
            this.fechaHora = new Date();
        }
        this.estado = estado;
        this.numProductos = numProductos;
        this.precioTotal = precioTotal;
        this.idCliente = idCliente;
    }

    public StringProperty getNumPedidoProperty() {
        return new SimpleStringProperty("#"+getIdPedido().toString());
    }
    
    public Integer getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(Integer idPedido) {
        this.idPedido = idPedido;
    }
    
    public StringProperty getFechaHoraProperty() {
        return new SimpleStringProperty(Util.obtenerFechaHora(getFechaHora()));
    }

    public Date getFechaHora() {
        return new Date(fechaHora.getTime());
    }

    public void setFechaHora(Date fechaHora) {
        if(fechaHora != null) {
            this.fechaHora = new Date(fechaHora.getTime());
        } else {
            this.fechaHora = new Date();
        }
    }
    
    public StringProperty getEstadoProperty() {
        return new SimpleStringProperty(getEstado());
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    public StringProperty getNumProductosProperty() {
        return new SimpleStringProperty(getNumProductos().toString()+" producto(s)");
    }

    public Integer getNumProductos() {
        return numProductos;
    }

    public void setNumProductos(Integer numProductos) {
        this.numProductos = numProductos;
    }
    
    public StringProperty getTotalProperty() {
        return new SimpleStringProperty("$"+getPrecioTotal().toString());
    }

    public Double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(Double precioTotal) {
        this.precioTotal = precioTotal;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }
    
    
}
