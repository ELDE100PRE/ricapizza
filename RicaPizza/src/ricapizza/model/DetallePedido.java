package ricapizza.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DetallePedido {
    private Integer idPedido;
    private String codigo;
    private Integer cantidad;
    private Double precioUnitario;

    public DetallePedido() {
    }

    public DetallePedido(Integer idPedido, String codigo, Integer cantidad, Double precioUnitario) {
        this.idPedido = idPedido;
        this.codigo = codigo;
        this.cantidad = cantidad;
        this.precioUnitario = precioUnitario;
    }

    public Integer getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(Integer idPedido) {
        this.idPedido = idPedido;
    }
    
    public StringProperty getCodigoProperty() {
        return new SimpleStringProperty(getCodigo());
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    public IntegerProperty getCantidadPoperty() {
        return new SimpleIntegerProperty(getCantidad());
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }
    
    public StringProperty getPrecioUnitarioProperty() {
        return new SimpleStringProperty("$"+getPrecioUnitario().toString());
    }

    public Double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(Double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }
    
    public String getSubtotal() {
        return "$" + (getPrecioUnitario() * getCantidad());
    }
    
    public StringProperty getSubtotalProperty() {
        return new SimpleStringProperty(getSubtotal());
    }
}