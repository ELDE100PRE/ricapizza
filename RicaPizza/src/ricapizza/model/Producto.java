package ricapizza.model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Producto {
    private final StringProperty codigo;
    private Double cantidad;
    private final StringProperty nombre;
    private String descripcion;
    private final DoubleProperty precio;
    private String restricciones;
    private final IntegerProperty inventario;

    public Producto() {
        this.codigo = new SimpleStringProperty();
        this.cantidad = null;
        this.nombre = new SimpleStringProperty();
        this.descripcion = null;
        this.precio = new SimpleDoubleProperty();
        this.restricciones = null;
        this.inventario = new SimpleIntegerProperty();
    }

    public Producto(String codigo, Double cantidad, String nombre, String descripcion, Double precio, String restricciones, Integer inventario) {
        this.codigo = new SimpleStringProperty(codigo);
        this.cantidad = cantidad;
        this.nombre = new SimpleStringProperty(nombre);
        this.descripcion = descripcion;
        this.precio = new SimpleDoubleProperty(precio);
        this.restricciones = restricciones;
        this.inventario = new SimpleIntegerProperty(inventario);
    }
    
    public StringProperty getCodigoProperty() {
        return codigo;
    }
    
    public String getCodigo() {
        return codigo.getValue();
    }
    
    public void setCodigo(String codigo) {
        this.codigo.setValue(codigo);
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }
    
    public StringProperty getNombreProperty() {
        return nombre;
    }
    
    public String getNombre() {
        return nombre.getValue();
    }
    
    public void setNombre(String nombre) {
        this.nombre.setValue(nombre);
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public DoubleProperty getPrecioProperty() {
        return precio;
    }
    
    public Double getPrecio() {
        return precio.getValue();
    }
    
    public void setPrecio(Double precio) {
        this.precio.setValue(precio);
    }

    public String getRestricciones() {
        return restricciones;
    }

    public void setRestricciones(String restricciones) {
        this.restricciones = restricciones;
    }
    
    public IntegerProperty getInventarioProperty() {
        return inventario;
    }
    
    public Integer getInventario() {
        return inventario.getValue();
    }
    
    public void setInventario(Integer inventario) {
        this.inventario.setValue(inventario);
    }

    @Override
    public String toString() {
        return getCodigo() + " - " + getNombre();
    }
}