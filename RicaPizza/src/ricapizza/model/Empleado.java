package ricapizza.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Empleado {
    private Integer idEmpleado;
    private final StringProperty nombres;
    private final StringProperty apellidos;
    private final StringProperty telefono;
    private final StringProperty email;
    private final StringProperty direccion;
    private final IntegerProperty codigopostal;
    private final StringProperty ciudad;
    private String contrasenia;

    public Empleado() {
        this.idEmpleado = null;
        this.nombres = new SimpleStringProperty();
        this.apellidos = new SimpleStringProperty();
        this.telefono = new SimpleStringProperty();
        this.email = new SimpleStringProperty();
        this.direccion = new SimpleStringProperty();
        this.codigopostal = new SimpleIntegerProperty();
        this.ciudad = new SimpleStringProperty();
        this.contrasenia = null;
    }

    public Empleado(Integer idEmpleado, String nombres, String apellidos, String telefono, String email, String direccion, Integer codigopostal, String ciudad, String contrasenia) {
        this.idEmpleado = idEmpleado;
        this.nombres = new SimpleStringProperty(nombres);
        this.apellidos = new SimpleStringProperty(apellidos);
        this.telefono = new SimpleStringProperty(telefono);
        this.email = new SimpleStringProperty(email);
        this.direccion = new SimpleStringProperty(direccion);
        this.codigopostal = new SimpleIntegerProperty();
        this.codigopostal.setValue(codigopostal);
        this.ciudad = new SimpleStringProperty(ciudad);
        this.contrasenia = contrasenia;
    }

    public Integer getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(Integer idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public StringProperty getNombresProperty() {
        return nombres;
    }
    
    public String getNombres() {
        return nombres.getValue();
    }

    public void setNombres(String nombres) {
        this.nombres.setValue(nombres);
    }
    
    public StringProperty getApellidosProperty() {
        return apellidos;
    }

    public String getApellidos() {
        return apellidos.getValue();
    }

    public void setApellidos(String apellidos) {
        this.apellidos.setValue(apellidos);
    }
    
    public StringProperty getTelefonoProperty() {
        return telefono;
    }

    public String getTelefono() {
        return telefono.getValue();
    }

    public void setTelefono(String telefono) {
        this.telefono.setValue(telefono);
    }
    
    public StringProperty getEmailProperty() {
        return email;
    }

    public String getEmail() {
        return email.getValue();
    }

    public void setEmail(String email) {
        this.email.setValue(email);
    }
    
    public StringProperty getDireccionProperty() {
        return direccion;
    }

    public String getDireccion() {
        return direccion.getValue();
    }

    public void setDireccion(String direccion) {
        this.direccion.setValue(direccion);
    }
    
    public IntegerProperty getCodigopostalProperty() {
        return codigopostal;
    }

    public Integer getCodigopostal() {
        return codigopostal.getValue();
    }

    public void setCodigopostal(Integer codigopostal) {
        this.codigopostal.setValue(codigopostal);
    }
    
    public StringProperty getCiudadProperty() {
        return ciudad;
    }

    public String getCiudad() {
        return ciudad.getValue();
    }

    public void setCiudad(String ciudad) {
        this.ciudad.setValue(ciudad);
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    } 
}