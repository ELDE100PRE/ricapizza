package ricapizza;

import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class RicaPizza extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        stage.getIcons().add(new Image(RicaPizza.class.getResourceAsStream("img/icono.png")));
        Util.login(stage);
    }

    public static void main(String[] args) {
        launch(args);
    }
}