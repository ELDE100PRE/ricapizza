package ricapizza;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import ricapizza.controller.ClientesController;
import ricapizza.controller.DetallePedidoController;
import ricapizza.controller.EmpleadosController;
import ricapizza.controller.LoginController;
import ricapizza.controller.MenuController;
import ricapizza.controller.PedidosController;
import ricapizza.controller.ProductosController;
import ricapizza.model.Empleado;
import ricapizza.model.Pedido;

public final class Util {
    public static boolean esNumero(String numero) {
        return numero.matches("\\d+");
    }
    public static boolean esNumeroDecimal(String numero) {
        if(numero.matches("\\d+\\.\\d+")) {
            return (numero.chars().filter(ch -> ((char) ch) == '.').count() == 1);
        } else {
            return false;
        }
    }
    public static boolean esCorreo(String correo) {
        if(correo.matches(".+@.+")) {
            return (correo.chars().filter(ch -> ((char) ch) == '@').count() == 1);
        } else {
            return false;
        }
    }
    public static boolean esDireccion(String numero) {
        return (numero.matches(".*\\d+.*") && numero.matches(".*\\D+.*"));
    }
    public static boolean esFecha(String fecha) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            format.setLenient(false);
            format.parse(fecha);
            return true;
        } catch (ParseException pEx) {
            return false;
        }
    }
    public static String obtenerFechaHora(Date fechaHora) {
        return new SimpleDateFormat("dd/MM/yyyy HH:mm a").format(fechaHora);
    }
    public static Optional<ButtonType> confirmacion(String titulo, String mensaje) {
        Alert confirmacion = new Alert(Alert.AlertType.CONFIRMATION);
        confirmacion.setTitle(titulo);
        confirmacion.setHeaderText(null);
        confirmacion.setContentText(mensaje);
        confirmacion.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
        Button yesButton = (Button) confirmacion.getDialogPane().lookupButton(ButtonType.YES);
        yesButton.setDefaultButton(false);
        Button noButton = (Button) confirmacion.getDialogPane().lookupButton(ButtonType.NO);
        noButton.setDefaultButton(true);
        return confirmacion.showAndWait();
    }
    public static void dialogo(Alert.AlertType tipo, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setHeaderText(null);
        alert.setContentText(mensaje);
        alert.show();
    }
    public static void excepcion(Exception exception) {
        if(exception.getMessage().contains("Communications link failure")) {
            Util.dialogo(AlertType.ERROR, "No se puede establecer una conexión con la base de datos, intente más tarde");
            return;
        }
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Excepción");
        alert.setHeaderText(null);
        alert.setContentText(exception.getLocalizedMessage());
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);
        String exceptionText = sw.toString();
        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);
        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);
        alert.getDialogPane().setExpandableContent(textArea);
        alert.showAndWait();
    }
    public static void login(Stage stage) {
        try {
            FXMLLoader loader = new FXMLLoader(RicaPizza.class.getResource("view/Login.fxml"));
            Parent root = (Parent) loader.load();
            LoginController controller = (LoginController) loader.getController();
            controller.setStage(stage);
            Scene scene = new Scene(root);
            stage.hide();
            stage.setScene(scene);
            stage.setTitle("Rica Pizza - Inicio de sesión");
            stage.setResizable(false);
            stage.setMaximized(false);
            stage.sizeToScene();
            stage.show();
        } catch (IOException ioEx) {
            Util.excepcion(ioEx);
        }
    }
    public static void menu(Stage stage, Empleado empleado) {
        try {
            FXMLLoader loader = new FXMLLoader(RicaPizza.class.getResource("view/Menu.fxml"));
            Parent root = (Parent) loader.load();
            MenuController controller = (MenuController) loader.getController();
            controller.setStage(stage);
            controller.setEmpleado(empleado);
            Scene scene = new Scene(root);
            stage.hide();
            stage.setScene(scene);
            stage.setTitle("Rica Pizza - Menu principal");
            stage.setResizable(false);
            stage.setMaximized(false);
            stage.sizeToScene();
            stage.show();
        } catch (IOException ioEx) {
            Util.excepcion(ioEx);
        }
    }
    public static void empleados(Stage stage, Empleado empleado) {
        try {
            FXMLLoader loader = new FXMLLoader(RicaPizza.class.getResource("view/Empleados.fxml"));
            Parent root = (Parent) loader.load();
            EmpleadosController controller = (EmpleadosController) loader.getController();
            controller.setStage(stage);
            controller.setEmpleadoLogin(empleado);
            Scene scene = new Scene(root);
            stage.hide();
            stage.setScene(scene);
            stage.setTitle("Rica Pizza - Administración de empleados");
            stage.setResizable(true);
            stage.sizeToScene();
            stage.show();
        } catch (IOException ioEx) {
            Util.excepcion(ioEx);
        }
    }
    public static void clientes(Stage stage, Empleado empleado) {
        try {
            FXMLLoader loader = new FXMLLoader(RicaPizza.class.getResource("view/Clientes.fxml"));
            Parent root = (Parent) loader.load();
            ClientesController controller = (ClientesController) loader.getController();
            controller.setStage(stage);
            controller.setEmpleado(empleado);
            Scene scene = new Scene(root);
            stage.hide();
            stage.setScene(scene);
            stage.setTitle("Rica Pizza - Administración de clientes");
            stage.setResizable(true);
            stage.sizeToScene();
            stage.show();
        } catch (IOException ioEx) {
            Util.excepcion(ioEx);
        }
    }
    public static void productos(Stage stage, Empleado empleado) {
        try {
            FXMLLoader loader = new FXMLLoader(RicaPizza.class.getResource("view/Productos.fxml"));
            Parent root = (Parent) loader.load();
            ProductosController controller = (ProductosController) loader.getController();
            controller.setStage(stage);
            controller.setEmpleado(empleado);
            Scene scene = new Scene(root);
            stage.hide();
            stage.setScene(scene);
            stage.setTitle("Rica Pizza - Administración de productos");
            stage.setResizable(true);
            stage.sizeToScene();
            stage.show();
        } catch (IOException ioEx) {
            Util.excepcion(ioEx);
        }
    }
    public static void pedidos(Stage stage, Empleado empleado) {
        try {
            FXMLLoader loader = new FXMLLoader(RicaPizza.class.getResource("view/Pedidos.fxml"));
            Parent root = (Parent) loader.load();
            PedidosController controller = (PedidosController) loader.getController();
            controller.setStage(stage);
            controller.setEmpleado(empleado);
            Scene scene = new Scene(root);
            stage.hide();
            stage.setScene(scene);
            stage.setTitle("Rica Pizza - Administración de pedidos");
            stage.setResizable(true);
            stage.sizeToScene();
            stage.show();
        } catch (IOException ioEx) {
            Util.excepcion(ioEx);
        }
    }
    public static void detallepedido(Stage stage, Empleado empleado, Pedido pedido) {
        try {
            FXMLLoader loader = new FXMLLoader(RicaPizza.class.getResource("view/DetallePedido.fxml"));
            Parent root = (Parent) loader.load();
            DetallePedidoController controller = (DetallePedidoController) loader.getController();
            controller.setStage(stage);
            controller.setEmpleado(empleado);
            controller.setPedido(pedido);
            Scene scene = new Scene(root);
            stage.hide();
            stage.setScene(scene);
            stage.setTitle("Rica Pizza - Pedido #"+pedido.getIdPedido().toString());
            stage.setResizable(true);
            stage.sizeToScene();
            stage.show();
        } catch (IOException ioEx) {
            Util.excepcion(ioEx);
        }
    }
}
